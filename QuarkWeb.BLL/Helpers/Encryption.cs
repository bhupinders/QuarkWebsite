﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuarkWeb.BLL.Helpers
{
    public class Encryption
    {
        private static string key = "tu89geji340t89u2";

        public static string Encrypt(string input)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return String2HEX(Convert.ToBase64String(resultArray, 0, resultArray.Length));
        }

        public static string Decrypt(string input)
        {
            //input = input.Replace("%555", "/");
            string asciString = HEXString2ASCII(input);

            if (string.IsNullOrEmpty(asciString))
            {
                return string.Empty;
            }
            byte[] inputArray = Convert.FromBase64String(asciString);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string String2HEX(string str)
        {
            try
            {
                string sHEX = "";

                for (int i = 0; i < str.Length; i++)
                {
                    sHEX = sHEX + Convert.ToInt32(str[i]).ToString("x");
                }
                return sHEX;
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        public static string HEXString2ASCII(string hexString)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= hexString.Length - 2; i += 2)
                {
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), NumberStyles.HexNumber))));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
