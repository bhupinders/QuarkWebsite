﻿using System.Collections.Generic;
using System.Linq;
using BLLEnums = QuarkWeb.BLL.Enumerators;

namespace QuarkWeb.BLL.Enumerators
{
    public enum QueryStatus
    {
        OK = 1,
        Error
    }
}

namespace QuarkWeb.BLL
{
    public class QueryResult
    {
        public QueryResult()
        {
            Messages = new List<string>();
            Status = BLLEnums.QueryStatus.Error;
        }

        public long Id { get; set; }

        public BLLEnums.QueryStatus Status { get; set; }

        public List<string> Messages { get; set; }

        public string GetMessages
        {
            get
            {
                return Messages.Count > 0 ? string.Join("<br>", Messages.Select(x => x.ToString()).ToArray()) : "";
            }
        }
    }
}