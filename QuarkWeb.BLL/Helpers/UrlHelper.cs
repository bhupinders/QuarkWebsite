﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarkWeb.BLL.Helpers
{
    public static class UrlHelper
    {
        public static Uri GetCurrentUrl()
        {
            var protocol = System.Web.HttpContext.Current.Request.Url.Scheme;
            var host = System.Web.HttpContext.Current.Request.Url.Host;
            var port = System.Web.HttpContext.Current.Request.Url.Port;
            var page = System.Web.HttpContext.Current.Request.Url.PathAndQuery;

            var currentUri = new UriBuilder(protocol, host, port, page);
            return currentUri.Uri;
        }

    }
}
