﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuarkWeb.BLL.Models;
using BLLModels = QuarkWeb.BLL.Models.Entities;

namespace QuarkWeb.BLL.IRepositories
{
    public interface IUserRepository
    {
        Models.Entities.AspNetUser Get(string Id);
        QueryResult Save(BLLModels.JobApplication model);
        QueryResult DeleteJob(int id, string jobid);
        List<BLLModels.JobApplication> GetAll();
        BLLModels.JobApplication GetJob(int id, string jobid);
    }
}
