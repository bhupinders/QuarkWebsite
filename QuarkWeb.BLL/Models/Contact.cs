﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace QuarkWeb.BLL.Models
{
  public  class Contact
    {
        public Contact()
        {
            ID = Guid.NewGuid();
        }
        [Required]
        public Guid ID { get; private set; }

        [Required(ErrorMessage = "Name is required.")]
        [StringLength(20, ErrorMessage = "Name cannot be longer than 20 characters.")]
        public string FullName {get;set;}
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Required")]
        [StringLength(14, MinimumLength = 10, ErrorMessage = "Invalid")]
        public string Number { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Message { get; set; }
        
    }
}
