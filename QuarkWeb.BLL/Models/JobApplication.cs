﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuarkWeb.BLL.Models.Entities
{
   public class JobApplication
    {
        public JobApplication()
        {
            JobID = Guid.NewGuid();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public Guid JobID { get; private set; }

        [Required(ErrorMessage = "Possition is required.")]
        [StringLength(50, ErrorMessage = "Possition cannot be longer than 50 characters.")]
        public string Possition { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "Description cannot be longer than 300 characters.")]      
        public string Description { get; set; }

        [Required(ErrorMessage = "Experience is required")]
        [StringLength(50, ErrorMessage = "experience cannot be longer than 50 characters.")]
        public string Experience { get; set; }

        [Required(ErrorMessage = "salary is required")]
        [StringLength(20, ErrorMessage = "salary cannot be longer than 20 characters.")]
        public string Salary { get; set; }

       

    }
}
