﻿namespace QuarkWeb.BLL.Models
{
    public class JsonResponse
    {
        public bool Success { get; set; }
        public object Response { get; set; }
        public string Errors { get; set; }

        public object ToJson() {

            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
       
    }
}
