﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarkWeb.BLL.Models
{
    public class QuarkViewModal
    {
        public BLL.Models.Contact cont { get; set; }
        public List<BLL.Models.Entities.JobApplication> job {get ; set;}
    }
}
