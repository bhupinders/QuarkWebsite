﻿using QuarkWeb.BLL.IRepositories;
using System;
using BLLModels = QuarkWeb.BLL.Models;
using QuarkWeb.BLL.Models.Entities;
using BLLEnums = QuarkWeb.BLL.Enumerators;
using System.Linq;
using System.Collections.Generic;

namespace QuarkWeb.BLL.Services
{
    public class UserServices : IUserRepository
    {

        public IUserRepository Account { get; private set; }
        private QueryResult result = new QueryResult();
        public UserServices(IUserRepository user)
        {
            Account = user;
        }


        public BLLModels.Entities.AspNetUser Get(string Userid)
        {
            return Account.Get(Userid);
        }

        public JobApplication save()
        {
            throw new NotImplementedException();
        }

        public QueryResult Save(JobApplication model)
        {
            try
            {
                result = Account.Save(model);
                if (result.Status == BLLEnums.QueryStatus.Error)
                {
                    result.Messages.Add("unable to save");
                }

                return result;
            }
            catch (Exception ex)
            {               
                result.Messages.Add("unable to save"); 
            }
            return result;
        }
        public List <JobApplication> GetAll()
        {
            return Account.GetAll();
        }
        public JobApplication GetJob(int id, string jobid)
        {
            return Account.GetJob(id, jobid);
        }
        public QueryResult DeleteJob(int id, string jobid)
        {
            return Account.DeleteJob(id, jobid);
        }
    }
}
