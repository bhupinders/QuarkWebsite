﻿using BllModels = QuarkWeb.BLL.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Security.Cryptography;
using QuarkWeb.BLL.IRepositories;
using QuarkWeb.BLL;
using QuarkWeb.BLL.Models.Entities;
using System;
using BLLEnums = QuarkWeb.BLL.Enumerators;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;

namespace QuarkWeb.DAL.SQLServer
{
    public class UserRepository : IUserRepository
    {
       
        public BllModels.Entities.AspNetUser Get(string Userid)
        {
            using (var db = new QuarkWebContext())
            {
                return null;
            }
        }
        public List<JobApplication> GetAll()
        {
            List<JobApplication> model = new List<JobApplication>();
            using (var context = new QuarkWebContext())
            {
                context.Database.Connection.Open();
                model = context.JobApplication.SqlQuery("SELECT * FROM dbo.JobApplications").ToList();
            }
            return model;
        }
        public JobApplication GetJob(int id, string jobid)
        {
          JobApplication model = new JobApplication();
            using (var context = new QuarkWebContext())
            {
                model = context.JobApplication.SqlQuery(string.Format("SELECT * FROM dbo.JobApplications where id={0} and jobid='{1}'",id,jobid)).Single();
            }
            return model;
        }


        public QueryResult Save(JobApplication model)
        {           
                using (var db = new QuarkWebContext())
                {
                    db.JobApplication.Add(model);
                    try
                    {
                        db.SaveChanges();
                        return new QueryResult
                        {
                            Status = BLLEnums.QueryStatus.OK
                        };
                    }
                    catch (Exception ex)
                    {

                        return new QueryResult
                        {
                            Status = BLLEnums.QueryStatus.Error
                        };
                    }
                    
                }
        }
        public QueryResult DeleteJob( int id, string jobid)
        {
            using (var db = new QuarkWebContext())
            {
                
                try
                {
                    var stud = (from s in db.JobApplication
                                where s.Id == id
                                select s).FirstOrDefault();

                    db.JobApplication.Remove(stud);
                    db.SaveChanges();
                    return new QueryResult
                    {
                        Status = BLLEnums.QueryStatus.OK
                    };
                }
                catch (Exception ex)
                {

                    return new QueryResult
                    {
                        Status = BLLEnums.QueryStatus.Error
                    };
                }

            }
        }
    }
}
