﻿using System.Web;
using System.Web.Optimization;

namespace QuarkWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/sliderjs").Include(

                      "~/Scripts/js/jquery.cookie.js", "~/Scripts/js/jquery-1.10.2.min.js",
                      "~/Scripts/js/crawler.js", "~/Scripts/js/templateemo_custom.js", "~/Scripts/js/jquery.cycle2.min.js",
                      "~/Scripts/js/jquery.cycle2.carousel.min.js", "~/Scripts/js/jquery-1.11.3.min.js", "~/Scripts/js/jssor.slider-22.2.0.mini.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css", "~/Content/css/templatemo_style.css", "~/Content/site.css"));

        }
    }
}
