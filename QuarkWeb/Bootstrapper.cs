﻿using StructureMap;
using StructureMap.Configuration.DSL;
using Repo = QuarkWeb.DAL.SQLServer;
using Services = QuarkWeb.BLL.Services;

namespace Quarkweb
{
    public static class Bootstrapper
    {
        public static void ConfigureStructureMap()
        {
            StructureMapConfiguration.AddRegistry(new AdminRegistry());
        }
    }

    public class AdminRegistry : Registry
    {
        protected override void configure()
        {
            //**Repositories
            ForRequestedType<QuarkWeb.BLL.IRepositories.IUserRepository>().TheDefaultIsConcreteType<Repo.UserRepository>();
           
            //**Services
            // ForRequestedType<Services.IUserServices>().TheDefaultIsConcreteType<Services.UserServices>();


        }

    }
}