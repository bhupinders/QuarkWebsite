﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuarkWeb.BLL.Models;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using QuarkWeb.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using BLLModels = QuarkWeb.BLL.Models.Entities;
using QuarkWeb.BLL;
using BLLServices = QuarkWeb.BLL.Services;

namespace QuarkWeb.Controllers
{
    [Authorize]
    public class IndexController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private QueryResult QueryResult = new QueryResult();
        private readonly QuarkWeb.BLL.IRepositories.IUserRepository Account = StructureMap.ObjectFactory.GetInstance<BLLServices.UserServices>();
        public IndexController()
        {
            Account = StructureMap.ObjectFactory.GetInstance<BLLServices.UserServices>();
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        // GET: Index
        [AllowAnonymous]
        public ActionResult Index()
        {
            List<BLLModels.JobApplication> model = new List<BLLModels.JobApplication>();
            BLL.Models.QuarkViewModal modelpass = new QuarkViewModal();          
            model = Account.GetAll();
            modelpass.job = model;
            ViewBag.jobApplication = model;
            return View();
        }
        [AllowAnonymous]
        public ActionResult Index2()
        {
            List<BLLModels.JobApplication> model = new List<BLLModels.JobApplication>();
            BLL.Models.QuarkViewModal modelpass = new QuarkViewModal();
            model = Account.GetAll();
            modelpass.job = model;
            ViewBag.jobApplication = model;
            return View();
        }
        [AllowAnonymous]
        public ActionResult ApplyJob(int id, string jobid)
        {
            BLLModels.JobApplication model = new BLLModels.JobApplication();
            model = Account.GetJob(id, jobid);
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult R7()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Residentials()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Vision()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Facilities()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult QuarkcityOverview()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Leasing()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Maintenance()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Sales()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        public ActionResult ChairmanMessage()
        {
            return View();
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        [AllowAnonymous]
        public ActionResult Offices()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Residential()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult EnvPolicy()
        {
            return View();
        }
        public ActionResult ChairmanDesk()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Layout()
        {
            return View();
        }
      //  [HttpPost]
         
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Index");
        }
        [HttpPost]
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignIn(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
           
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction("AdminIndex","Account");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View("Login",model);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult SendMessage(Contact model)
        {
            try
            {
                if (model.Email != null)
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["Host"].ToString());
                    mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"].ToString());
                    mail.To.Add(ConfigurationManager.AppSettings["MailTo"].ToString());                            // Sending MailTo
                    List<string> li = new List<string>();
                    li.Add("vchauhan@feindustriesindia.com");
                    mail.CC.Add(string.Join<string>(",", li));       // Sending CC
                    mail.Bcc.Add(string.Join<string>(",", li));      // Sending Bcc  
                    mail.Subject = model.Subject;                     // Mail Subject
                    mail.Body = "Meassage/ Query.\n\nFullName:" + " " + model.FullName + "\n\n " + "Email: " + " " + model.Email + "\n\n " + "Number: " + " " + model.Number + "\n\n " + "Subject: " + " " + model.Subject + "\n\n " + "Message: " + " " + model.Message + "\n\n " + "*****This is an automatically generated email, please do not reply ***";

                    SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]); //PORT
                    SmtpServer.EnableSsl = true;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new NetworkCredential("hrms.quarkcity@gmail.com", "Welcome@123#");
                    SmtpServer.Send(mail);
                    ModelState.Clear();
                    
                    ViewBag.response = "Message Send !";
                }
                else {
                    ModelState.Clear();
                    ViewBag.response = "";
                    return View("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.response = "";
                throw ex;
            }
           
            return View("Index");
            
        }
    }
}