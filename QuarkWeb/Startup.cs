﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuarkWeb.Startup))]
namespace QuarkWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
